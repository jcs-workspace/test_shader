/**
 * $File: Test.cs $
 * $Date: 2023-05-08 11:05:44 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2023 by Shen, Jen-Chieh $
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///
/// </summary>
public class Test : MonoBehaviour
{
    /* Variables */

    /* Setter & Getter */

    /* Functions */
    private void Update()
    {

    }
}
