Shader "Test_Shader/Test" {
    Properties {
        _MainTex ("Texture", 2D) = "white" {}
        _Value   ("Value", Float) = 1.0
        _ColorA ("Color A", Color) = (1, 1, 1, 1)
        _ColorB ("Color B", Color) = (1, 1, 1, 1)
        _ColorStart ("Color Start", Range(0 ,1)) = 1
        _ColorEnd ("Color End", Range(0 ,1)) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Transparent"
            "Queue"="Transparent"
        }
        LOD 100

        Pass {
            //Cull Back             // back space culling
            //Cull Front            // back space culling
            Cull Off
            ZWrite Off           // ignore depth buffer
            //ZTest Always
            Blend One One        // additive
            //Blend DstColor Zero  // multiply

            CGPROGRAM
#pragma vertex vert
#pragma fragment frag
            // make fog work
#pragma multi_compile_fog

#include "UnityCG.cginc"

#define TAU 6.28318530718

            float _Value;
            float4 _ColorA;
            float4 _ColorB;
            float _ColorStart;
            float _ColorEnd;

            struct MeshData {  // pre-vertex mesh data
                float4 vertex : POSITION;  // vertex position
                float3 normals: NORMAL;
                float2 uv0 : TEXCOORD0;     // UV coordinates
            };

            struct v2f {
                float4 vertex : SV_POSITION;
                float3 normal : TEXCOORD0;
                float2 uv : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            // Vertex shader
            v2f vert (MeshData v) {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.normal = UnityObjectToWorldNormal(v.normals);
                //o.normal = v.normals;
                o.uv = v.uv0;
                return o;
            }

            float InverseLerp(float a, float b, float v) {
                return (v - a) / (b - a);
            }

            // Fragment shader
            fixed4 frag (v2f i) : SV_Target {
                {
                    //float t = saturate(InverseLerp(_ColorStart, _ColorEnd, i.uv.x));
                    //float4 outColor = lerp(_ColorA, _ColorB, t);
                    //return outColor;
                }

                {
                    //return 1 - i.uv.y;

                    float xOffset = cos(i.uv.x * TAU * 8) * 0.01;
                    float deltaTime = _Time.y * 0.1;
                    float t = cos((i.uv.y + xOffset - deltaTime) * TAU * 5) * 0.5 + 0.5;
                    t *= 1 - i.uv.y;

                    float topBottomRemover = (abs(i.normal.y) < 0.999);
                    float waves = t * topBottomRemover;

                    float4 gradient = lerp(_ColorA, _ColorB, i.uv.y);

                    return gradient * waves;
                }

                {
                    //float4 outColor = float4(i.normal, 1);
                    //return outColor;
                }
            }

            ENDCG
        }
    }
}
