Shader "Unlit/VertexOffset" {
    Properties {
        _MainTex ("Texture", 2D) = "white" {}
        _WaveAmp ("Wave Amplitude", Range(0, 30)) = 1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
            //"Queue"="Geometry"
        }
        LOD 100

        Pass {
            CGPROGRAM
#pragma vertex vert
#pragma fragment frag
            // make fog work
#pragma multi_compile_fog

#include "UnityCG.cginc"

#define TAU 6.28318530718

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            float _WaveAmp;

            v2f vert (appdata v)
            {
                v2f o;

                float wavesY = cos((v.uv.y - _Time.y * 0.1)  * TAU * 5);
                float wavesX = cos((v.uv.x - _Time.y * 0.1)  * TAU * 5);

                float applyAmp = wavesY * wavesX * _WaveAmp;

                v.vertex.y = applyAmp;

                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target {

                float waves = cos((i.uv.y - _Time.y * 0.1)  * TAU * 5) * 0.5 + 0.5;
                //t *= 1 - i.uv.y;
                return waves;
            }
            ENDCG
        }
    }
}
