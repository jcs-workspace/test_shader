/**
 * $File: 03_Uniforms.shader $
 * $Date: 2023-04-19 17:38:34 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2023 by Shen, Jen-Chieh $
 */

Shader "thebookofshaders/03_Uniforms" {
    Properties {
        _MainTex ("Texture", 2D) = "white" {}
        _ColorA ("Color A", Color) = (1, 1, 1, 1)
        _ColorB ("Color B", Color) = (1, 1, 1, 1)
    }
    SubShader {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass {
            ZWrite Off
            Blend One One

            CGPROGRAM
#pragma vertex vert
#pragma fragment frag
            // make fog work
#pragma multi_compile_fog

#include "UnityCG.cginc"
#define TAU 6.28318530718

            struct appdata {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _ColorA;
            float4 _ColorB;

            float plot(float2 st, float pct) {
                return smoothstep(pct - 0.02, pct, st.y) - smoothstep(pct, pct + 0.02, st.y);
            }

            // Vertex shader
            v2f vert (appdata v) {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            // Fragment shader
            fixed4 frag (v2f i) : SV_Target {
                float3 color = float3(0, 0, 0);

                float pct = abs(sin(_Time.y));

                color = lerp(_ColorA, _ColorB, pct);

                return float4(color, 1);
            }

            float4 drawLine(v2f i) {
                float2 st = i.uv;

                float y = pow(st.x, 5.0);

                float3 color = float3(y, 1.0, 1.0);

                float pct = plot(st, y);
                color = (1.0 - pct) * color + pct * float3(0, 1, 0);
                return float4(color, 1.0);
            }

            float4 flashing() {
                float delta = abs(sin(_Time.y));
                float4 output = float4(delta, 0, 0, 1);
                return output;
            }
            ENDCG
        }
    }
}
