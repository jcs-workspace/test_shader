/**
 * $File: 07.shader $
 * $Date: 2023-04-22 13:44:03 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2023 by Shen, Jen-Chieh $
 */

Shader "thebookofshaders/07"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
#pragma vertex vert
#pragma fragment frag
            // make fog work
#pragma multi_compile_fog

#include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            appdata hole(appdata v)
            {
                float dist = distance(v.uv, float2(0.5, 0.5));
                v.vertex.y = smoothstep(0, 0.3, dist);
                return v;
            }

            v2f vert (appdata v)
            {
                v2f o;

                //v = hole(v);

                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            float4 square_around(v2f i)
            {
                float2 st = i.uv;
                float3 color = float3(0, 0, 0);

                float2 bl = smoothstep(float2(0.1, 0.1), float2(0.2, 0.2), st);
                float pct = bl.x * bl.y;

                float2 tr = smoothstep(float2(0.1, 0.1), float2(0.2, 0.2), 1.0 - st);
                pct *= tr.x * tr.y;

                color = float3(pct, pct, pct);

                return float4(color, 1.0);

                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }

            float4 peit_mondrian(v2f i) {
                float2 st = i.uv;
                float x = i.uv.x;
                float y = i.uv.y;
                float3 color = float3(1, 1, 1);

                float line1;

                if (x < 0.2 || x > 0.25)
                    line1 = 1;
                else
                    line1 = 0;

                float pct = line1;

                color = float3(pct, pct, pct);

                return float4(color, 1.0);
            }

            float4 circle(v2f i) {
                float2 st = i.uv;
                float pct = 1.0;
                float dist = distance(st, float2(0.5, 0.5));
                pct = smoothstep(0.0, 0.3, dist);
                float3 color = float3(pct, pct, pct);
                return float4(color, 0.0);
            }

            float4 dribble(v2f i) {
                float2 st = i.uv;
                float3 color = float3(0.0, 0.0, 0.0);
                float d = 0.0;

                st = st * 2.0 - 1.0;

                d = length(abs(st) - 0.3);

                float channel = frac(d * 10.0);

                return float4(float3(channel,
                                     channel,
                                     channel),
                              1.0);
            }

            float4 bufferfly(v2f i)
            {
                float st = i.uv;

                float2 pos = float2(0.5, 0.5) - st;
                float2 flipPos = float2(pos.y, pos.x);

                float r = length(pos) * 2.0;
                float a = atan(flipPos);

                float f = cos(a * 3.0);

                float v = 1. - smoothstep(f, f + 0.02, r);
                float3 color = float3(v, v, v);

                return float4(color, 1.0);
            }

            fixed4 frag (v2f i) : SV_Target
            {
                //return circle(i);
                //return peit_mondrian(i);
                //return square_around(i);
                //return dribble(i);
                return bufferfly(i);
            }
            ENDCG
        }
    }
}
